# frozen_string_literal: true

require 'spec_helper'

RSpec.describe GitlabSubscriptions::TrialsHelper, feature_category: :acquisition do
  using RSpec::Parameterized::TableSyntax
  include Devise::Test::ControllerHelpers

  describe '#create_lead_form_data' do
    let(:user) { build_stubbed(:user, user_detail: build_stubbed(:user_detail, organization: '_org_')) }

    let(:extra_params) do
      {
        first_name: '_params_first_name_',
        last_name: '_params_last_name_',
        company_name: '_params_company_name_',
        company_size: '_company_size_',
        phone_number: '1234',
        country: '_country_',
        state: '_state_'
      }
    end

    let(:params) do
      ActionController::Parameters.new(extra_params.merge(glm_source: '_glm_source_', glm_content: '_glm_content_'))
    end

    let(:eligible_namespaces) { [] }

    before do
      allow(helper).to receive(:params).and_return(params)
      allow(helper).to receive(:current_user).and_return(user)
    end

    subject(:form_data) { helper.create_lead_form_data(eligible_namespaces) }

    it 'provides expected form data' do
      keys = extra_params.keys + [:submit_path, :submit_button_text]

      expect(form_data.keys.map(&:to_sym)).to match_array(keys)
    end

    it 'allows overriding data with params' do
      expect(form_data).to match(a_hash_including(extra_params))
    end

    context 'when namespace_id is in the params' do
      let(:extra_params) { { namespace_id: non_existing_record_id } }

      it 'provides the submit path with the namespace_id' do
        expect(form_data[:submit_path]).to eq(trials_path(step: :lead, **params.permit!))
      end
    end

    context 'when params are empty' do
      let(:extra_params) { {} }

      it 'uses the values from current user' do
        current_user_attributes = {
          first_name: user.first_name,
          last_name: user.last_name,
          company_name: user.organization
        }

        expect(form_data).to match(a_hash_including(current_user_attributes))
      end
    end

    context 'when there are no eligible namespaces' do
      it 'has the Continue text' do
        expect(form_data).to match(a_hash_including(submit_button_text: s_('Trial|Continue')))
      end
    end

    context 'when there is a single eligible namespace' do
      let(:eligible_namespaces) { [build(:namespace)] }

      it 'has the Activate text' do
        expect(form_data).to match(a_hash_including(submit_button_text: s_('Trial|Activate my trial')))
      end
    end

    context 'when there are multiple eligible namespaces' do
      let(:eligible_namespaces) { build_list(:namespace, 2) }

      it 'has the Continue text' do
        expect(form_data).to match(a_hash_including(submit_button_text: s_('Trial|Continue')))
      end
    end
  end

  describe '#create_duo_pro_lead_form_data' do
    let(:user) { build_stubbed(:user, user_detail: build_stubbed(:user_detail, organization: '_org_')) }

    let(:extra_params) do
      {
        first_name: '_params_first_name_',
        last_name: '_params_last_name_',
        company_name: '_params_company_name_',
        company_size: '_company_size_',
        phone_number: '1234',
        country: '_country_',
        state: '_state_'
      }
    end

    let(:params) { ActionController::Parameters.new(extra_params) }
    let(:eligible_namespaces) { [] }

    before do
      allow(helper).to receive(:params).and_return(params)
      allow(helper).to receive(:current_user).and_return(user)
    end

    subject(:form_data) { helper.create_duo_pro_lead_form_data(eligible_namespaces) }

    it 'provides expected form data' do
      keys = extra_params.keys + [:submit_path, :submit_button_text]

      expect(form_data.keys.map(&:to_sym)).to match_array(keys)
    end

    it 'allows overriding data with params' do
      expect(form_data).to match(a_hash_including(extra_params))
    end

    context 'when namespace_id is in the params' do
      let(:extra_params) { { namespace_id: non_existing_record_id } }

      it 'provides the submit path with the namespace_id' do
        expect(form_data[:submit_path]).to eq(trials_duo_pro_path(step: :lead, **params.permit!))
      end
    end

    context 'when params are empty' do
      let(:extra_params) { {} }

      it 'uses the values from current user' do
        current_user_attributes = {
          first_name: user.first_name,
          last_name: user.last_name,
          company_name: user.organization
        }

        expect(form_data).to match(a_hash_including(current_user_attributes))
      end
    end

    context 'when there are no eligible namespaces' do
      it 'has the Continue text' do
        expect(form_data).to match(a_hash_including(submit_button_text: s_('Trial|Continue')))
      end
    end

    context 'when there is a single eligible namespace' do
      let(:eligible_namespaces) { [build(:namespace)] }

      it 'has the Activate text' do
        expect(form_data).to match(a_hash_including(submit_button_text: s_('Trial|Activate my trial')))
      end
    end

    context 'when there are multiple eligible namespaces' do
      let(:eligible_namespaces) { build_list(:namespace, 2) }

      it 'has the Continue text' do
        expect(form_data).to match(a_hash_including(submit_button_text: s_('Trial|Continue')))
      end
    end
  end

  describe '#create_duo_enterprise_lead_form_data' do
    let(:user) { build_stubbed(:user, user_detail: build_stubbed(:user_detail, organization: '_org_')) }

    let(:extra_params) do
      {
        first_name: '_params_first_name_',
        last_name: '_params_last_name_',
        company_name: '_params_company_name_',
        company_size: '_company_size_',
        phone_number: '1234',
        country: '_country_',
        state: '_state_'
      }
    end

    let(:params) { ActionController::Parameters.new(extra_params) }
    let(:eligible_namespaces) { [] }

    before do
      allow(helper).to receive(:params).and_return(params)
      allow(helper).to receive(:current_user).and_return(user)
    end

    subject(:form_data) { helper.create_duo_enterprise_lead_form_data(eligible_namespaces) }

    it 'provides expected form data' do
      keys = extra_params.keys + [:submit_path, :submit_button_text]

      expect(form_data.keys.map(&:to_sym)).to match_array(keys)
    end

    it 'allows overriding data with params' do
      expect(form_data).to match(a_hash_including(extra_params))
    end

    context 'when namespace_id is in the params' do
      let(:extra_params) { { namespace_id: non_existing_record_id } }

      it 'provides the submit path with the namespace_id' do
        expect(form_data[:submit_path]).to eq(trials_duo_enterprise_path(step: :lead, **params.permit!))
      end
    end

    context 'when params are empty' do
      let(:extra_params) { {} }

      it 'uses the values from current user' do
        current_user_attributes = {
          first_name: user.first_name,
          last_name: user.last_name,
          company_name: user.organization
        }

        expect(form_data).to match(a_hash_including(current_user_attributes))
      end
    end

    context 'when there are no eligible namespaces' do
      it 'has the Continue text' do
        expect(form_data).to match(a_hash_including(submit_button_text: s_('Trial|Continue')))
      end
    end

    context 'when there is a single eligible namespace' do
      let(:eligible_namespaces) { [build(:namespace)] }

      it 'has the Activate text' do
        expect(form_data).to match(a_hash_including(submit_button_text: s_('Trial|Activate my trial')))
      end
    end

    context 'when there are multiple eligible namespaces' do
      let(:eligible_namespaces) { build_list(:namespace, 2) }

      it 'has the Continue text' do
        expect(form_data).to match(a_hash_including(submit_button_text: s_('Trial|Continue')))
      end
    end
  end

  describe '#should_ask_company_question?' do
    let(:params) { ActionController::Parameters.new(url_params) }

    before do
      allow(helper).to receive(:params).and_return(params)
    end

    subject { helper.should_ask_company_question? }

    where(:url_params, :result) do
      { glm_source: 'about.gitlab.com' }  | false
      { glm_source: 'learn.gitlab.com' }  | false
      { glm_source: 'docs.gitlab.com' }   | false
      { glm_source: 'abouts.gitlab.com' } | true
      { glm_source: 'about.gitlab.org' }  | true
      { glm_source: 'about.gitlob.com' }  | true
      {}                                  | true
    end

    with_them do
      it { is_expected.to eq(result) }
    end
  end

  describe '#show_tier_badge_for_new_trial?' do
    where(:trials_available?, :paid?, :private?, :never_had_trial?, :authorized, :result) do
      false | false | true | true | true | false
      true | true | true | true | true | false
      true | false | false | true | true | false
      true | false | true | false | true | false
      true | false | true | true | false | false
      true | false | true | true | true | true
    end

    with_them do
      let(:namespace) { build(:namespace) }
      let(:user) { build(:user) }

      before do
        stub_saas_features(subscriptions_trials: trials_available?)
        allow(namespace).to receive(:paid?).and_return(paid?)
        allow(namespace).to receive(:private?).and_return(private?)
        allow(namespace).to receive(:never_had_trial?).and_return(never_had_trial?)
        allow(helper).to receive(:can?).with(user, :read_billing, namespace).and_return(authorized)
      end

      subject { helper.show_tier_badge_for_new_trial?(namespace, user) }

      it { is_expected.to be(result) }
    end
  end

  describe '#glm_source' do
    let(:host) { ::Gitlab.config.gitlab.host }

    it 'return gitlab config host' do
      glm_source = helper.glm_source

      expect(glm_source).to eq(host)
    end
  end

  describe '#namespace_options_for_listbox' do
    let_it_be(:group1) { create :group }
    let_it_be(:group2) { create :group }

    let(:trial_eligible_namespaces) { [] }

    let(:new_optgroup) do
      {
        text: _('New'),
        options: [
          {
            text: _('Create group'),
            value: '0'
          }
        ]
      }
    end

    let(:groups_optgroup) do
      {
        text: _('Groups'),
        options: trial_eligible_namespaces.map { |n| { text: n.name, value: n.id.to_s } }
      }
    end

    subject { helper.namespace_options_for_listbox(trial_eligible_namespaces) }

    context 'when there is no eligible group' do
      it 'returns just the "New" option group', :aggregate_failures do
        is_expected.to match_array([new_optgroup])
      end
    end

    context 'when only group namespaces are eligible' do
      let(:trial_eligible_namespaces) { [group1, group2] }

      it 'returns the "New" and "Groups" option groups', :aggregate_failures do
        is_expected.to match_array([new_optgroup, groups_optgroup])
        expect(subject[1][:options].length).to be(2)
      end
    end

    context 'when some group namespaces are eligible' do
      let(:trial_eligible_namespaces) { [group2] }

      it 'returns the "New", "Groups" option groups', :aggregate_failures do
        is_expected.to match_array([new_optgroup, groups_optgroup])
        expect(subject[1][:options].length).to be(1)
      end
    end
  end

  describe '#trial_selection_intro_text' do
    let(:namespace) { build(:namespace) }

    subject { helper.trial_selection_intro_text(namespaces) }

    where(:namespaces, :text) do
      [ref(:namespace)] | s_('Trials|You can apply your trial of Ultimate with GitLab Duo Enterprise to a group.')
      []                | s_('Trials|Create a new group and start your trial of Ultimate with GitLab Duo Enterprise.')
    end

    with_them do
      it { is_expected.to eq(text) }
    end
  end

  describe '#trial_namespace_selector_data' do
    let(:parsed_selector_data) { Gitlab::Json.parse(selector_data[:items]) }

    subject(:selector_data) { helper.trial_namespace_selector_data(eligible_namespaces, nil) }

    context 'when there are eligible namespaces' do
      let(:namespace) { build(:namespace) }
      let(:eligible_namespaces) { [namespace] }

      it 'returns selector data with the eligible namespace' do
        group_options = [{ 'text' => namespace.name, 'value' => namespace.id.to_s }]

        is_expected.to include(any_trial_eligible_namespaces: 'true')
        new_group_option = parsed_selector_data[0]['options']
        group_select_options = parsed_selector_data[1]['options']
        expect(new_group_option).to eq([{ 'text' => _('Create group'), 'value' => '0' }])
        expect(group_select_options).to eq(group_options)
      end
    end

    context 'when there are no eligible namespaces' do
      let(:eligible_namespaces) { [] }

      it 'returns selector data with only create option' do
        is_expected.to include(any_trial_eligible_namespaces: 'false')
        new_group_option = parsed_selector_data[0]['options']
        group_select = parsed_selector_data[1]
        expect(new_group_option).to eq([{ 'text' => _('Create group'), 'value' => '0' }])
        expect(group_select).to be_nil
      end
    end
  end

  describe '#duo_trial_namespace_selector_data' do
    let(:parsed_selector_data) { Gitlab::Json.parse(selector_data[:items]) }

    subject(:selector_data) { helper.duo_trial_namespace_selector_data(eligible_namespaces, nil) }

    context 'when there are eligible namespaces' do
      let(:namespace) { build(:namespace) }
      let(:eligible_namespaces) { [namespace] }

      it 'returns selector data with the eligible namespace' do
        is_expected.to include(any_trial_eligible_namespaces: 'true')
        expect(parsed_selector_data).to eq([{ 'text' => namespace.name, 'value' => namespace.id.to_s }])
      end
    end

    context 'when there are no eligible namespaces' do
      let(:eligible_namespaces) { [] }

      it 'returns empty selector data' do
        is_expected.to include(any_trial_eligible_namespaces: 'false')
        expect(parsed_selector_data).to be_empty
      end
    end
  end

  describe '#trial_form_errors_message' do
    let(:result) { ServiceResponse.error(message: ['some error']) }

    subject { helper.trial_form_errors_message(result) }

    it 'returns error message from the result directly' do
      is_expected.to eq('some error')
    end

    context 'when the error has :generic_trial_error as reason' do
      let(:result) do
        ServiceResponse.error(message: ['some error'],
          reason: GitlabSubscriptions::Trials::BaseApplyTrialService::GENERIC_TRIAL_ERROR)
      end

      it 'overrides the error message' do
        is_expected.to include('Please try again or reach out to')
        is_expected.to include(
          '<a target="_blank" rel="noopener noreferrer" href="https://support.gitlab.com/hc/en-us">GitLab Support</a>'
        )
      end
    end
  end
end
